# ASP_Restaurant

## General info
Restaurant website offering home ordering options. Orders and users are saved to the SQL database.

## Technologies
* ASP.NET
* C#
* Bootstrap.js
* SQL

## Status
Project is:  _finished_
*Server is off