﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_Restaurant.Models
{
    [Bind(Exclude = "Id")]
    public class CustomerOrder
    {

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane")]
        [DisplayName("Imie")]
        [StringLength(160)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        [DisplayName("Nazwisko")]
        [StringLength(160)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Adres jest wymagany")]
        [StringLength(70)]
        [DisplayName("Adres")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Miejscowość jest wymagana")]
        [StringLength(40)]
        [DisplayName("Miasto")]
        public string City { get; set; }
        [DisplayName("Kod pocztowy")]
        public string PostalCode { get; set; }
        [Required(ErrorMessage = "Telefon wymagany")]
        [StringLength(24)]
        [DisplayName("Telefon")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Email jest wymagany")]
        [DisplayName("Adres Email")]

        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
            ErrorMessage = "Email nie jest prawidłowy")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [ScaffoldColumn(false)]
        [Column(TypeName = "datetime2")]
        public DateTime DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public Decimal Amount { get; set; }

        [ScaffoldColumn(false)]
        public string CustomerUserName { get; set; }

    }
}